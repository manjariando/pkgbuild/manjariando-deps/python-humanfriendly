# Maintainer: Maikel Wever <maikelwever@gmail.com>
# Maintainer: Matthew Gamble <git@matthewgamble.net>

pkgname=python-humanfriendly
pkgver=10.0
pkgrel=2
pkgdesc="Human friendly input/output in Python"
arch=('any')
url="https://github.com/xolox/${pkgname}"
license=('MIT')
makedepends=('python-setuptools' 'python-sphinx')
source=("https://github.com/xolox/${pkgname}/archive/${pkgver}.tar.gz"
        "python3.10.patch::https://github.com/xolox/python-humanfriendly/commit/e1baf63e8e33ac85ae9d06e52d1ba331fb73452d.diff")
sha256sums=('a7f6ee6aa93933ffdf716a44163a8b1d17e8c95b3badb25efa37d562b2b93393'
            '646bbbf6132ce15f5f64418d63d0ece0bf3e6d80b45d5786e08e666d4d71dbc8')

build() {
  cd "${srcdir}/${pkgname}-${pkgver}"

  python setup.py build

  cd docs
  sphinx-build -nb html -d build/doctrees . build/html
}

package() {
  depends=('python')

  cd "${srcdir}/${pkgname}-${pkgver}"

  PYTHONHASHSEED=0 python setup.py install --root="${pkgdir}" --optimize=1 --skip-build
  install -Dm644 LICENSE.txt "${pkgdir}/usr/share/licenses/${pkgname}/LICENSE.txt"
  install -Dm644 CHANGELOG.rst "${pkgdir}/usr/share/doc/${pkgname}/CHANGELOG.rst"
  cp -r docs/build/html "${pkgdir}/usr/share/doc/${pkgname}/html"
}
